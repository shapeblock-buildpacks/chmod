# Chmod buildpack
This is a fork of https://github.com/eirini-forks/chmod-buildpack which is tailored to address permissions issues for
apps running on Jammy stacks.

It is intended to be a short-term fix and not meant to be used generically.

## Build the buildpack

```bash
pack buildpack package registry.gitlab.com/shapeblock-buildpacks/chmod:0.0.1 --config ./package.toml --publish
```

```yaml
permissions:
  - path: /foo
    mode: a+w
  - path: /bar/a.txt
    mode: 400
```    